from django.urls import path

from . import views

app_name = 'DataPasien'

urlpatterns = [
    path('add/', views.add, name='add'),
    path('listofpasien/', views.listofpasien, name='listofpasien'),
    path('edit/<str:pk>/', views.edit, name='update'),
    path('delete/<str:pk>/', views.hapus, name='delete'),
    path('sembuh/<str:pk>/', views.tambahsembuh, name='sembuh'),
    path('meninggal/<str:pk>/', views.tambahmeninggal, name='meninggal'),
    path('counter/', views.counter, name='counter'),
    path('patients/', views.get_patient, name='patients'),
    
]
