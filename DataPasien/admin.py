from django.contrib import admin
from .models import Pasien, Sembuh, Meninggal

# Register your models here.
admin.site.register(Pasien)
admin.site.register(Sembuh)
admin.site.register(Meninggal)