from django.shortcuts import render, redirect
from .forms import Patient
from .models import Pasien, Sembuh, Meninggal
from django.http.response import HttpResponseRedirect
from django.http import JsonResponse

# Create your views here.

def add(request):
    form = Patient()
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = Patient(request.POST)
            if form.is_valid():
                form.save()
        return render(request, 'DataPasien/add.html', {'form':form})
    else:
        return HttpResponseRedirect('/login')

def hapus(request, pk):
    yangdiapus = Pasien.objects.get(id=pk)
    if request.method == 'POST':
        yangdiapus.delete()
        return redirect('/listofpasien/')
    return render(request, 'DataPasien/hapus.html', {'yangdiapus':yangdiapus})


def edit(request, pk):
    yangdiedit = Pasien.objects.get(id=pk)
    form = Patient(instance=yangdiedit)

    if request.method == 'POST':

        form = Patient(request.POST, instance=yangdiedit)

        if form.is_valid():
            form.save()
            return redirect('/listofpasien/')

    return render(request, 'DataPasien/edit.html', {'form':form })



def listofpasien(request):
    pasien2 = Pasien.objects.all()
    # return JsonResponse(list(pasien2.values()), safe=False)
    return render(request, 'DataPasien/listofpasien.html', {'pasien2':pasien2})

def counter(request):
    jmlsembuh = Sembuh.objects.all().count()
    jumlmeninggal = Meninggal.objects.all().count()
    pasienpos = Pasien.objects.filter(status='Positif').count()
    return render (request, 'DataPasien/counter.html', {'sembuh':jmlsembuh,'positif':pasienpos,'meninggal':jumlmeninggal})

def tambahsembuh(request, pk):
    yangsembuh = Pasien.objects.get(id=pk)
    if yangsembuh.status == 'Negatif':
        return redirect('/listofpasien/')
    if request.method == 'POST':
        Sembuh.objects.create(jml=1)
        yangsembuh.status = 'Negatif'
        return redirect('/listofpasien/')
    return render(request, 'DataPasien/sembuh.html', {'yangdiapus':yangsembuh})

def tambahmeninggal(request, pk):
    yangmeninggal = Pasien.objects.get(id=pk)
    if yangmeninggal.status == 'Negatif':
        return redirect('/listofpasien/')
    if request.method == 'POST':
        Meninggal.objects.create(jml=1)
        yangmeninggal.delete()
        return redirect('/listofpasien/')
    return render(request, 'DataPasien/meninggal.html', {'yangdiapus':yangmeninggal})

def get_patient(request):
    pasien2 = Pasien.objects.all()
    return JsonResponse(list(pasien2.values()), safe=False)

