from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post,Comment
from .forms import Form_Post, Form_Comment
from django.core import serializers
from django.http import HttpResponse

# Create your views here.
def forum(request):
    post = Post.objects.all()
    return render (request,'forum.html',{'post':post})

def addpost(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = Form_Post(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect ('/forum')
            else:
                return HttpResponseRedirect('Data tidak Valid')

        form = Form_Post()
        tipe = 'Post'
        tipe2= 'Forum'
        return render(request,'add.html',{'form':form, 'tipe':tipe, 'tipe2':tipe2})
    else:
        return HttpResponseRedirect('/login')

def komen(request,id):
    post = Post.objects.get(id=id)
    komen = Comment.objects.filter(post=post)
    return render(request,'detail.html',{'post':post,'komen':komen})
    

def addkomen(request,id):
    if request.user.is_authenticated:
        post = Post.objects.get(id=id)
        komen = Comment.objects.filter(post=post)
        form = Form_Comment()
        tipe = 'Comment'
        tipe2= 'The Post'
        if request.method == "POST":
            form = Form_Comment(request.POST)
            if form.is_valid():
                komen = form.save(commit=False)
                komen.post = post
                komen.save()
                return HttpResponseRedirect ('/comment/'+ str(post.id))
            else:
                return HttpResponseRedirect('Data tidak Valid')
        
        return render(request,'add.html',{'form':form,'tipe':tipe, 'tipe2':tipe2})
    else:
        return HttpResponseRedirect('/login')

def hapuspost(request,id):
    if request.user.is_authenticated:
        post = Post.objects.get(id=id)
        post.delete()
        return HttpResponseRedirect('/forum')
    else:
        return HttpResponseRedirect('/login')

def hapuskomen(request,id):
    if request.user.is_authenticated:
        komen = Comment.objects.get(id=id)
        komen.delete()
        return HttpResponseRedirect('/comment/'+str(komen.post.id))
    else:
        return HttpResponseRedirect('/login')

def posts(request):
    text = request.GET.get('q','')
    posts = Post.objects.filter(judul__icontains=text).order_by('-tanggal_post')
    post_list = serializers.serialize('json', posts)
    return HttpResponse(post_list, content_type="application/json")


