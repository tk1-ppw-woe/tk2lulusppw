from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import forum,addpost, komen,addkomen
from .forms import Form_Post, Form_Comment
from .models import Post, Comment
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import request

# Create your tests here.
class TestForum(TestCase):
    def test_url_forum(self):
        response = Client().get('/forum')
        self.assertEqual(response.status_code, 200)
    
    def test_url_data(self):
        response = Client().get('/datapost')
        self.assertEqual(response.status_code, 200)

    def test_url_add_post(self):
        response = Client().get('/addpost')
        self.assertEqual(response.status_code, 302)
    
    def test_template_add(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)
        response = self.client.get('/addpost')
        self.assertTemplateUsed(response,'add.html')

    def test_template_forum(self):
        response = Client().get('/forum')
        self.assertTemplateUsed(response, 'forum.html')
    
    def test_func_forum(self):
        found = resolve('/forum')
        self.assertEqual(found.func, forum)

    def test_func_addpost(self):
        found = resolve('/addpost')
        self.assertEqual(found.func, addpost)
  
    def test_addpost_success_and_show(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        response_post = self.client.post('/addpost', {'nama': test, 'judul':test, 'pesan':test})
        self.assertEqual(response_post.status_code, 302)

        response= self.client.get('/forum')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_addpost_logout(self):
        test = 'protes'
        response_post = Client().post('/addpost', {'nama': '','judul':'','pesan':''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/forum')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_addpost_failed(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        response_post = self.client.post('/addpost', {'nama': '', 'judul':'', 'pesan':''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/forum')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_url_show_post_and_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        response = Client().get('/comment/' + str(post.id) )
        self.assertEqual(response.status_code, 200)

    def test_url_add_comment_login(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        response = self.client.get('/addcomment/' + str(post.id) )
        self.assertEqual(response.status_code, 200)


        

    def test_func_show_post_and_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        found = resolve('/comment/' + str(post.id))
        self.assertEqual(found.func, komen)

    def test_func_add_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test,judul= test, pesan=test)
        found = resolve('/addcomment/' + str(post.id))
        self.assertEqual(found.func, addkomen)

    def test_template_show_post_and_comment(self):
        test = 'protes'
        post = Post.objects.create(nama= test, judul= test, pesan=test)
        response = Client().get('/comment/' + str(post.id) )
        self.assertTemplateUsed(response, 'detail.html')

    def test_template_add_comment(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        post = Post.objects.create(nama= test,judul= test, pesan=test)
        response = self.client.get('/addcomment/' + str(post.id) )
        self.assertTemplateUsed(response, 'add.html')

    def test_addcomment_success_and_show(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        post = Post.objects.create(nama= 'abc', judul= 'abc', pesan='abc')
        response_post = self.client.post('/addcomment/'+str(post.id), {'nama': test,'komentar':test})
        self.assertEqual(response_post.status_code, 302)

        response= self.client.get('/comment/'+str(post.id))
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_addcomment_login_failed(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)
        test = 'protes'
        post = Post.objects.create(nama = 'abc', judul='abc', pesan='abc')
        response_post = self.client.post('/addcomment/'+str(post.id), {'nama': '','komentar':''})
        self.assertEqual(response_post.status_code, 302)
    
        response= self.client.get('/comment/'+str(post.id))
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_addcomment_failed(self):
        test = 'protes'
        post = Post.objects.create(nama = 'abc', judul='abc', pesan='abc')
        response_post = Client().post('/addcomment/'+str(post.id), {'nama': '','komentar':''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/comment/'+str(post.id))
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_delete_post_success(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        post = Post.objects.create(nama = test, judul=test, pesan=test)
        response= self.client.get('/hapuspost/'+str(post.id))
        jumlah = Post.objects.all().count()
        self.assertEqual(0,jumlah)

    def test_delete_post_failed(self):
        test = 'protes'
        post = Post.objects.create(nama = test, judul=test, pesan=test)
        response= self.client.get('/hapuspost/'+str(post.id))
        jumlah = Post.objects.all().count()
        self.assertEqual(1,jumlah)

    def test_delete_comment_success(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)

        test = 'protes'
        post = Post.objects.create(nama = test, judul=test, pesan=test)
        komen= Comment.objects.create(post=post, nama=test, komentar="test")
        response= self.client.get('/hapuskomen/'+str(komen.id))
        jumlah = Comment.objects.all().count()
        self.assertEqual(0,jumlah)

    def test_delete_comment_failed(self):
        test = 'protes'
        post = Post.objects.create(nama = test, judul=test, pesan=test)
        komen= Comment.objects.create(post=post, nama=test, komentar="test")
        response= self.client.get('/hapuskomen/'+str(komen.id))
        jumlah = Comment.objects.all().count()
        self.assertEqual(1,jumlah)



  





