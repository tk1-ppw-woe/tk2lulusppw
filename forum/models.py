from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Post(models.Model):
    nama = models.CharField(max_length=30,default="Anonymous")
    judul = models.CharField(max_length=90)
    pesan = models.TextField(max_length=280)
    tanggal_post = models.DateTimeField(default=timezone.now)
class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    nama = models.CharField(max_length=30)
    komentar = models.TextField(max_length=280)
    tanggal_komen = models.DateTimeField(default=timezone.now)
