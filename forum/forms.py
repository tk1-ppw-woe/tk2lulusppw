from django import forms
from .models import Post, Comment


class Form_Post(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['nama','judul','pesan']
        widgets = {
            'nama': forms.TextInput(attrs={'class':'form-control','placeholder':'Nama'}),
            'judul': forms.TextInput(attrs={'class':'form-control','placeholder':'Judul'}),
            'pesan': forms.Textarea(attrs={'class':'form-control','placeholder':'Pesan'}),
        }
        
class Form_Comment(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['nama','komentar']
        widgets = {
            'nama': forms.TextInput(attrs={'class':'form-control','placeholder':'Nama'}),
            'komentar': forms.Textarea(attrs={'class':'form-control','placeholder':'Komentar'}),
        }
        




