from django.test import TestCase,Client
from django.urls import resolve
from info.views import halaman_input,buka_halaman_info,halaman_hapus,cari_info,hapus,halaman_info
from info.models import Profile
import json

# Create your tests here.
class TestInfo(TestCase):
    def test_url_formpegawai(self):
        response = Client().get('/formpegawai/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_info(self):
        response = Client().get('/info/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_formhapus(self):
        response = Client().get('/formhapus/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_cariinfo(self):
        response = Client().get('/cariinfo')
        self.assertEqual(response.status_code,200)
    
    def test_url_hapus(self):
        response = Client().get('/hapus/')
        self.assertEqual(response.status_code,200)
    
    def test_url_ubahinfo(self):
        response = Client().get('/ubahinfo/')
        self.assertEqual(response.status_code,200)
    
    

    def test_template_info(self):
        response = Client().get('/info/')
        self.assertTemplateUsed(response, 'hasil.html') 
    def test_template_formhapus(self):
        response = Client().get('/formhapus/')
        self.assertTemplateUsed(response, 'hapus.html')
    def test_template_formpegawai(self):
        response = Client().get('/formpegawai/')
        self.assertTemplateUsed(response, 'info.html')
    def test_template_hapus(self):
        response = Client().get('/hapus/')
        self.assertTemplateUsed(response,'hasil.html')
    def test_template_ubahinfo(self):
        response = Client().get('/ubahinfo/')
        self.assertTemplateUsed(response, 'hasil.html')


    def test_func_info(self):
        found = resolve('/info/')
        self.assertEqual(found.func, buka_halaman_info)
    def test_func_hapus(self):
        found = resolve('/hapus/')
        self.assertEqual(found.func, hapus)
    def test_func_formhapus(self):
        found = resolve('/formhapus/')
        self.assertEqual(found.func, halaman_hapus)
    def test_func_ubahinfo(self):
        found = resolve('/ubahinfo/')
        self.assertEqual(found.func, halaman_info)
    def test_func_formpegawai(self):
        found = resolve('/formpegawai/')
        self.assertEqual(found.func, halaman_input)
    def test_func_cariinfo(self):
        found = resolve('/cariinfo')
        self.assertEqual(found.func, cari_info)
    
    def test_formpegawai_success_and_show(self):
        test = 'protes'
        post=Profile.objects.create(nama=test,alamat=test,no_telp=test)
        response_post = self.client.post('/formpegawai/', {'nama': test, 'alamat':test, 'no_telp':test})
        self.assertEqual(response_post.status_code, 200)
        response= self.client.get('/info/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_formpegawai_failed(self):
        test = 'protes'
        response_post = self.client.post('/addpost', {'nama': '', 'alamat':'', 'no_telp':''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/info/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    def test_hapus_success(self):
        test = 'protes'
        post = Profile.objects.create(nama = test, alamat=test, no_telp=test)
        response= self.client.post('/hapus/',{'nama':test})
        jumlah = Profile.objects.all().count()
        self.assertEqual(0,jumlah)
        response_test = self.client.get('/info/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_hapus_fail(self):
        test = 'protes'
        post = Profile.objects.create(nama = test, alamat=test, no_telp=test)
        response= self.client.post('/hapus/',{'nama':' '})
        jumlah = Profile.objects.all().count()
        self.assertEqual(1,jumlah)
    
    def test_cariinfo_success(self):
        test = 'protes'
        post = Profile.objects.create(nama=test,alamat=test,no_telp=test)
        response = self.client.post('/cariinfo?q='+test)
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(test,html_response)

    def create_whatever(self, x="only a test", y="yes, this is only a test",z="xx"):
        return Profile.objects.create(nama=x, alamat=y, no_telp=z)

    def test_whatever_creation(self):
        w = self.create_whatever()
        self.assertTrue(isinstance(w, Profile))
        self.assertEqual("only a test", w.nama)


