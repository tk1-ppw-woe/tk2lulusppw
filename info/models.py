from django.db import models
from cloudinary.models import CloudinaryField
# Create your models here.

class Profile(models.Model):
    nama = models.CharField(max_length=50)
    alamat = models.CharField(max_length=100)
    no_telp = models.CharField(max_length=12)
    foto = CloudinaryField('foto')

    def __str__(self):
        return self.nama
    @property
    def image_url(self):
        if self.foto and hasattr(self.foto, 'url'):
            return self.foto.url
