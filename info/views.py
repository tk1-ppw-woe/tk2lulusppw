from django.shortcuts import render,redirect
from info.models import Profile
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def halaman_input(request):
    return render(request,'info.html')

def halaman_info (request):
    try:
        y= request.FILES['foto']
    except:
        y= "s1alod90etuup3fz9zlw"
    count=0
    if request.method=='POST':
        for e in Profile.objects.filter(nama=request.POST['nama']):
            count+=1
        if count == 0:
            Profile.objects.create(nama=request.POST['nama'],alamat=request.POST['alamat'],no_telp=request.POST['no_telp'], foto=y)
            return redirect ('info:info')
    all_profile = Profile.objects.all()
    response = {'all_profile':all_profile}
    return render(request,'hasil.html',response)

def halaman_hapus(request):
    return render(request,'hapus.html')

def hapus(request):
    if request.method == 'POST':
        Profile.objects.filter(nama=request.POST['nama']).delete()
    all_profile = Profile.objects.all()
    response = {'all_profile':all_profile}
    return render(request,'hasil.html',response)

def buka_halaman_info(request):
    all_profile = Profile.objects.all()
    response = {'all_profile':all_profile}
    return render(request, 'hasil.html',response)

def cari_info(request):
    text = request.GET.get('q','')
    all_profile = Profile.objects.filter(nama__icontains=text)
    isi_info = serializers.serialize('json', all_profile)
    return HttpResponse(isi_info, content_type="application/json")