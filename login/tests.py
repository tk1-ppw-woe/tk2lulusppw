from django.test import TestCase, Client
from django.urls import reverse, resolve
from login.forms import *
from .models import *
from .views import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db import IntegrityError
from django.contrib import messages
from django.contrib.auth import SESSION_KEY
# Create your tests here.

class loginTestCase(TestCase):
    def test_url_login(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_signup(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        response = Client().get('/logout/', follow= True)
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
        # send login data
        response = self.client.post('/login/', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)

    def test_login_failed(self):
        response = self.client.post('/login/', {'username':'ngaco','password':'makinngaco',}, follow=True)
        # should be logged in now
        self.assertFalse(response.context['user'].is_active)

    def test_signup(self):
        test = 'a@gmail.com'
        response = Client().post('/signup/', {'username': test, 'email':test, 'password':test}, follow=True)
        users = User.objects.all()
        self.assertEqual(len(users),1)

    def test_signup_failed(self):
        test = 'a@gmail.com'
        response = Client().post('/signup/', {'username': '', 'email':test, 'password':test}, follow=True)
        users = User.objects.all()
        self.assertEqual(len(users),0)