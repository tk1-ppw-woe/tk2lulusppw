from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

app_name = "pengumuman"

urlpatterns = [
    path('pengumuman/', views.lihat_pengumuman , name='lihat_pengumuman'),
    path('tambah/', views.tambah_pengumuman, name = 'tambah_pengumuman'),
    path('hapus/<str:id>/', views.hapus_pengumuman, name = 'hapus_pengumuman'),
    path('data',views.data,name="data")
]

