from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views
from .models import Pengumuman
from django.contrib.auth.models import User

class TestViews(TestCase):
    def test_url_pengumuman(self):
        response = Client().get('/pengumuman/')
        self.assertEqual(response.status_code, 200)

    def test_template_pengumuman(self):
        response = Client().get('/pengumuman/')
        self.assertTemplateUsed(response,'pengumuman.html')
    
    def test_url_tambah_pengumuman(self):
        response = Client().get('/tambah/')
        self.assertEqual(response.status_code, 302)

    def test_template_tambah_pengumuman_authen(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)
        response = self.client.get('/tambah/')
        self.assertTemplateUsed(response,'tambah_pengumuman.html')

    def test_func_pengumuman(self):
        found = resolve('/pengumuman/')
        self.assertEqual(found.func , views.lihat_pengumuman)

    def test_func_tambah(self):
        found = resolve('/tambah/')
        self.assertEqual(found.func , views.tambah_pengumuman)

    def test_str_models(self):
        judul = "LIBUR HARI NATAL"
        buatobjek = Pengumuman.objects.create(judul=judul, detail = "akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", penting = True)
        self.assertEqual(buatobjek.__str__(), judul )


    def test_template_hasil_buat_pengumuman_berhasil(self):
        buatobjek = Pengumuman.objects.create(judul = "LIBUR HARI NATAL", detail = "akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", penting = True)
        hasil_hitung = Pengumuman.objects.all().count()
        self.assertEqual(hasil_hitung,1)
        new_response = self.client.get('/pengumuman/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("LIBUR HARI NATAL", html_response)
        self.assertIn("akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", html_response)

    def test_func_buat_pengumuman_not_authen(self):
        response = self.client.post(reverse("pengumuman:tambah_pengumuman"), {
            "judul" : "LIBUR HARI NATAL", "detail" :"akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", 'penting': True
        }, follow=True)
        hasil_hitung = Pengumuman.objects.all().count()
        self.assertEqual(hasil_hitung,0)

    def test_func_hapus_pengumuman_not_authen(self):
        self.buatobjek = Pengumuman.objects.create(judul = "LIBUR", detail = "akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", penting = True)
        self.test_delete_url = reverse("pengumuman:hapus_pengumuman",args=[str(self.buatobjek.id)])
        response = self.client.get(self.test_delete_url, follow=True)
        hitunglagi = Pengumuman.objects.all().count()
        self.assertEqual(hitunglagi,1)

    def test_func_buat_pengumuman_authen(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)
        response = self.client.post(reverse("pengumuman:tambah_pengumuman"), {
            "judul" : "LIBUR HARI NATAL", "detail" :"akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", 'penting': True
        }, follow=True)
        hasil_hitung = Pengumuman.objects.all().count()
        self.assertEqual(hasil_hitung,1)

    
    def test_func_hapus_pengumuman_authen(self):
        data = 'abcd'
        obj = User.objects.create(username=data)
        obj.set_password(data)
        obj.save()
        self.client.login(username=data, password=data)
        self.buatobjek = Pengumuman.objects.create(judul = "LIBUR", detail = "akan ada cuti bersama dari selama seminggu dari tanggal 23 Desember", penting = True)
        self.test_delete_url = reverse("pengumuman:hapus_pengumuman",args=[str(self.buatobjek.id)])
        response = self.client.get(self.test_delete_url, follow=True)
        hitunglagi = Pengumuman.objects.all().count()
        self.assertEqual(hitunglagi,0)
    
    def test_url_json(self):
        response = Client().get(reverse('pengumuman:data'))
        self.assertEqual(response.status_code, 200)
