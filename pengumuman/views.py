from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render,redirect
from .models import Pengumuman
from . import forms
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.core import serializers

def tambah_pengumuman(request):
    form = forms.Input_Form()
    if request.user.is_authenticated:
        if (request.method == "POST"):
            form = forms.Input_Form(request.POST)
            if form.is_valid():
                form.save()
                return redirect('pengumuman:lihat_pengumuman')
        response = {
            'form' : form
        }
        return render(request,'tambah_pengumuman.html',response)
    else:
        return  HttpResponseRedirect('/login')


def lihat_pengumuman(request):
    list_pengumuman = Pengumuman.objects.all()
    response = {
        'list' : list_pengumuman
    }
    return render(request,'pengumuman.html',response)


def hapus_pengumuman(request,id):
    if request.user.is_authenticated:
        Pengumuman.objects.get(id=id).delete()
        return redirect('pengumuman:lihat_pengumuman')
    else:
         return  HttpResponseRedirect('/login')

def data(request):
    text = request.GET.get('q','')
    posts = Pengumuman.objects.filter(judul__icontains=text).order_by('-tanggal')
    post_list = serializers.serialize('json', posts)
    return HttpResponse(post_list, content_type="application/json")
